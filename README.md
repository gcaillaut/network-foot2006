# NCAAF 2006 network

The communities (in file `data/reference2006.csv`) are manually scrapped from https://www.espn.com/college-football/standings/_/season/2006

The edges (in file `data/edges2006.csv`) are automatically scrapped from https://www.espn.com/college-football/schedule/_/year/2006. The file `scrap_html.R` contains functions to automate the retrieval of the html pages and to scrap them.

This is the network used in
- [Extraction de communautés ego-centrées par apprentissage supervisé d'espaces prétopologiques](https://dblp.org/rec/conf/f-egc/CaillautCD19)
- [Learning Pretopological Spaces to Extract Ego-Centered Communities](https://dblp.org/rec/conf/pakdd/CaillautCD19)